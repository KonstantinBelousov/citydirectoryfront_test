import React from "react";
/*import s from './Weather.module.css';*/
import Input from "./input/Input";
import InfoWeather from "./infoWeather/infoWeather";

class Weather extends React.Component {

  API_KEY = "105522893c90749b32e266347a41b760";
  /*const API_KEY = "b41b79516e1a647fca6bac5a9c98c48e";*/
  /*const API_KEY = "73d34f2396630c2ff3fa7956af4d85d6";*/

  state = {
    temp: undefined,
    temp_feels_like: undefined,
    name: undefined,
    country: undefined,
    sunrise: undefined,
    sunset: undefined,
    wind: undefined,
    deg: undefined,
    error: undefined,
  }

  getWeather = async (e) => {
    e.preventDefault();
    const cityName = e.target.elements.city.value;
    if (cityName) {
      const api_url = await
        fetch(`https://api.openweathermap.org/data/2.5/weather?q=${cityName}&APPID=${this.API_KEY}&units=metric`);
      const dataWeather = await api_url.json();
      console.log(dataWeather);

      let pressureRS = Math.round(dataWeather.main.pressure/1.333);

      let directionOfTheWind = '';
      switch (true) {
        case dataWeather.wind.deg <= 22.5 || dataWeather.wind.deg > 337.5:
          directionOfTheWind = "северный";
          break;
        case dataWeather.wind.deg > 22.5 && dataWeather.wind.deg <= 67.5:
          directionOfTheWind = "северо-восточный";
          break;
        case dataWeather.wind.deg > 67.5 && dataWeather.wind.deg <= 112.5:
          directionOfTheWind = "восточный";
          break;
        case dataWeather.wind.deg > 112.5 && dataWeather.wind.deg <= 157.5:
          directionOfTheWind = "юго-восточный";
          break;
        case dataWeather.wind.deg > 157.5 && dataWeather.wind.deg <= 202.5:
          directionOfTheWind = "южный";
          break;
        case dataWeather.wind.deg > 202.5 && dataWeather.wind.deg <= 247.5:
          directionOfTheWind = "юго-западный";
          break;
        case dataWeather.wind.deg > 247.5 && dataWeather.wind.deg <= 292.5:
          directionOfTheWind = "западный";
          break;
        case dataWeather.wind.deg > 292.5 && dataWeather.wind.deg <= 337.5:
          directionOfTheWind = "Северо-западный";
          break;
        default:
          directionOfTheWind = "некорректтные данные";
      }

      this.setState({
        temp: dataWeather.main.temp,
        temp_feels_like: dataWeather.main.feels_like,
        name: dataWeather.name,
        country: dataWeather.sys.country,
        wind: dataWeather.wind.speed,
        deg: directionOfTheWind,
        pressure: pressureRS,
        error: "",
      })
    }
  }

  render() {
    return (
      <div>
        <Input getWeather={this.getWeather}/>
        <InfoWeather name={this.state.name}
                     temp_feels_like={this.state.temp_feels_like}
                     temp={this.state.temp}
                     country={this.state.country}
                     wind={this.state.wind}
                     deg={this.state.deg}
                     pressure={this.state.pressure}
                     error={this.state.error}/>
      </div>
    )
  }
}

export default Weather;
