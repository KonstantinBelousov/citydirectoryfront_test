import React from "react";
import s from './Input.module.css';

const Input = (props) => {
    return (
      <form onSubmit={props.getWeather}>
          <input type={'text'} className={s.weatherInput} name={'city'} placeholder={'город'}/>
          <button type="submit">Получит погоду</button>
      </form>
    )
}

export default Input;
