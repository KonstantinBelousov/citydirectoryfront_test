import React from "react";
/*import s from './Input.module.css';*/

const InfoWeather = (props) => {
  console.log('props.name', props.name);
  return (
    <div>
      {props.temp &&
      <div>
        <p>{props.name}, {props.country}</p>
        <p>Температура: {props.temp} C</p>
        <p>Ощущается как: {props.temp_feels_like} C</p>
        <p>Давление: {props.pressure} мм рт. ст.</p>
        <p>Скорость ветра: {props.wind} м/c</p>
        <p>Направление ветра: {props.deg}</p>
      </div>
      }

    </div>
  )
}

export default InfoWeather;
