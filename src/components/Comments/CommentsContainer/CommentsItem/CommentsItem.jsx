import React from "react";

const CommentsContainer = (props) => {

  let commentsElements = props.state.comments.map(ce => <CommentItem name={ce.userName} text={ce.text}/>)

    return (
      <div>
        Здесь будут комментарии
        {commentsElements}
      </div>
    )
}

export default CommentsContainer;
