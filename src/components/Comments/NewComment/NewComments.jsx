import React from "react";
import CommentsContainer from "./CommentsContainer/CommentsContainer";
import c from './Comments.module.css';

const Comments = (props) => {
    return (
      <div className={c.container}>
        Комментарии:
        <CommentsContainer state={props.state}/>
      </div>
    )
}

export default Comments;
