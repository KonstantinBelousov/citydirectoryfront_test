import './App.css';
import React from "react";
import {BrowserRouter} from "react-router-dom";
import Weather from "./components/weather/Weather";

const App = (props) => {
  return (
    <BrowserRouter>
      <div className={'app-wrapper'}>
        <Weather/>
      </div>
    </BrowserRouter>
  );
}

export default App;
